# [Edwin Robotics Bluetooth Module HC-06](https://shop.edwinrobotics.com/bluetooth/602-edwin-robotics-bluetooth-module-hc-06.html) #
![Bluetooth Module small.png](https://bitbucket.org/repo/Rb8nEA/images/3140301048-Bluetooth%20Module%20small.png)

The Edwin Robotics HC-06 Bluetooth module comes with 300mA 3.3V voltage Regulator and level shifting circuits for a 5v tolerant communication between Arduino and Bluetooth module. The board is reverse current protected, thus preventing the board from getting damaged, the board comes with programmable LED, which can be turned off with simple serial interface and AT Commands. 

**FEATURES:**

* 5v tolerant communication.
* Reverse current protected
* Serial interface over RX and TX lines.
* Onboard programmable LED
* Power Status LED
* Jumper to disable LED's and bypass power supply.

**SPECIFICATION:**

* Model No: HC-06
* Mode: Slave
* Operating Voltage: 3v3 ~ 5V
* Communication Mode: Serial
* Bluetooth version: v2.0 + EDR
* Firmware: hc01.comV2.0 (It does not use Linvor firmware)
* Default baud rate: 9600
* Default PIN: 1234
* Default Name: HC-06
* Status LED: Blue (Top Left)
* Power LED: Red (Bottom Right)
* Dimensions: 16.5 x 41.5 mm

**DOCUMENTS:**

[Hookup Guide](http://learn.edwinrobotics.com/hc-06-bluetooth-module-hookup-guide/)