/**********************************************************************************
AT_Command_Interface.ino

Description:
  Sample AT Command Interface code for Arduino and HC06 Bluetooth Module

Tutorial Link:  
  http://learn.edwinrobotics.com/hc-06-bluetooth-module-hookup-guide/

Created
Abhishek Nair @ Edwin Robotics
Feb 9th 2017

Distributed as-is; no warranty is given.   
**********************************************************************************/
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3); // RX, TX

void setup() {

Serial.begin(9600);
Serial.println("------------------------------------------------------------------------------------------");
Serial.println("             Edwin Robotics HC06 Bluetooth Module Serial Interface ");
Serial.println("------------------------------------------------------------------------------------------");
Serial.println("                 Connect Bluetooth Module RX -> Arduino pin 3 ");
Serial.println("                 Connect Bluetooth Module TX -> Arduino pin 2 ");
Serial.println("------------------------------------------------------------------------------------------");
Serial.println("");
Serial.println("Entered AT command Mode, please Enter AT Commands from the list of supported commands:");
Serial.println("******************************************************************************************");
Serial.println("AT              > connection test command. Returns OK");
Serial.println("");
Serial.println("AT+BAUDx        > sets the baud rate to respective board rate for entered 'x' value(refer values below) and returns OKBaud_Rate");
Serial.println("                   1: 1200  2: 2400  3: 4800  4: 9600  5: 19200  6: 38400  7: 57600  8: 115200  9: 230400  A: 460800  B: 921600  C: 1382400");
Serial.println("");
Serial.println("AT+NAME(myName) > sets the name to (myName), returns OKsetname");
Serial.println("");
Serial.println("AT+PIN9999      > changes the PIN to 9999, returns OKsetPIN");
Serial.println("");
Serial.println("AT+VERSION      > returns the firmware version : hc01.comV2.0");
Serial.println("");
Serial.println("AT+LED0         > turn off the blue LED, returns LED OFF");
Serial.println("");
Serial.println("AT+LED1         > turn on the blue LED, returns LED ON");
Serial.println("");
Serial.println("AT+PN           > sets no parity");
Serial.println("");
Serial.println("AT+PE           > sets even parity");
Serial.println("");
Serial.println("AT+PO           > sets odd parity");
Serial.println("******************************************************************************************");
Serial.println("");

mySerial.begin(9600);
delay(300);
Serial.println();
Serial.println("Note: if 'Version: hc01.comV2.0' is not printed below, make sure that your connections are correct or your module is not connected to any other device,try restarting the device/Serial Window");
Serial.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
Serial.print("Version: ");
mySerial.write("AT+VERSION");
}

void loop()
{
  if (mySerial.available())
    Serial.write(mySerial.read());

  if (Serial.available())
    mySerial.write(Serial.read());
} 
